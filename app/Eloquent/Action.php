<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Action extends Model
{
    use SoftDeletes;

	public $incrementing = false;
	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'member_id', 'actionable_id', 'actionable_type', 'result'
	];

	public function member()
	{
		return $this->belongsTo(Member::class, 'member_id', 'id');
	}

	public function actionable()
	{
		return $this->morphTo();
	}
}
