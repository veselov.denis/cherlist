<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
	use SoftDeletes;

	const JOIN_TYPE_REQUEST = 'request';
	const JOIN_TYPE_JOIN = 'join';
	const JOIN_TYPE_APPROVED = 'approved';
	const JOIN_TYPE_ACCEPTED = 'accepted';

	const LEAVE_TYPE_SELF = 'self';
	const LEAVE_TYPE_ROBOT = 'robot';
	const LEAVE_TYPE_ADMIN = 'admin';

	public $incrementing = false;
	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'first_name', 'last_name', 'city', 'needed_check', 'join_type', 'is_member', 'leave_type', 'leave_reason', 'leaved_at', 'is_banned'
	];

	public function posts()
	{
		return $this->hasMany(Post::class, 'author_id', 'id');
	}

	public function comments()
	{
		return $this->hasMany(Comment::class, 'author_id', 'id');
	}

	public function actions()
	{
		return $this->hasMany(Action::class, 'member_id', 'id');
	}
}
