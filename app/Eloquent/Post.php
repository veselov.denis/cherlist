<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;

	public $incrementing = false;
	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'text', 'author_id'
	];

	public function author()
	{
		return $this->belongsTo(Member::class, 'author_id', 'id');
	}

	public function comments()
	{
		return $this->hasMany(Comment::class, 'post_id', 'id');
	}

	public function actions()
	{
		return $this->morphMany(Action::class, 'actionable');
	}
}
