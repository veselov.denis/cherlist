<?php

namespace App\Models;


use App\Eloquent\Comment;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class Wall extends Vk
{
	public static function addPost($msg)
	{
		$request_param = [
			'owner_id' => Config::get('const.GROUP_ID'),
			'message' => $msg,
			'v' => Config::get('const.API_V_5_63'),
			'access_token' => Config::get('const.TOKEN_ROBOT'),
		];

		self::request($request_param, 'wall.post');
	}

	/**
	 * Add comment to post
	 * @param $msg
	 * @param $post_id
	 * @return mixed
	 */
	public static function addComment($msg, $post_id)
	{
		$request_params = [
			'post_id' => $post_id,
			'from_group' => 0,
			'message' => $msg,
			'owner_id' => -Config::get('const.GROUP_ID'),
			'v' => Config::get('const.API_V_5_63'),
			'access_token' => Config::get('const.TOKEN_ROBOT')
		];

		return self::request($request_params, 'wall.createComment');
	}

	/**
	 * Delete post by id
	 * @param $post_id
	 */
    public static function deletePost($post_id)
	{
		$request_params = [
			'owner_id' => -Config::get('const.GROUP_ID'),
			'post_id' => $post_id,
			'v' => Config::get('const.API_V_5_63'),
			'access_token' => Config::get('const.TOKEN_ROBOT')
		];

		self::request($request_params, 'wall.delete');
	}

	/**
	 * Delete comment by id
	 * @param $comment_id
	 */
    public static function deleteComment($comment_id)
	{
		$request_params = [
			'owner_id' => -Config::get('const.GROUP_ID'),
			'comment_id' => $comment_id,
			'v' => Config::get('const.API_V_5_63'),
			'access_token' => Config::get('const.TOKEN_ROBOT')
		];

		self::request($request_params, 'wall.deleteComment');
	}

	/**
	 * Has the text hashtag or not
	 * @param $text
	 * @return bool
	 */
	public static function hasHashtag($text)
	{
		$text = Str::lower($text);

		$cats = self::getCategories();
		$cats = collect($cats)->values()->flatten();

		$cats->transform(
			function ($cat) {
				$cat = trim($cat);
				$cat = str_replace(',', '', $cat);
				$cat = str_replace("- ", " ", $cat);
				$cat = str_replace("-", " ", $cat);
				$cat = str_replace(" ", "_", $cat);
				$cat = trim($cat);
				$cat = Str::lower($cat);
				$cat = "#$cat@cherlist";
				return $cat;
			}
		);

		$has = Str::contains($text, $cats->toArray());
		return $has;
	}

	/**
	 * Has the text any links or not
	 * @param $text
	 * @return false|int
	 */
	public static function hasLinks($text)
	{
		return (bool)preg_match('~https?|www\.|\.ru|\.su|\.com|\.net|\.info|\.us~siu', $text);
	}

	/**
	 * Get posts data
	 * @param $post_ids
	 * @return mixed
	 */
	public static function getPostsData($post_ids)
	{
		$posts = collect($post_ids);

		$posts->transform(
			function ($post_id) {
				return -Config::get('const.GROUP_ID') . '_' . $post_id;
			}
		);

		$posts = $posts->implode(',');

		$request_params = [
			'posts' => $posts,
			'extends' => 1,
			'v' => Config::get('const.API_V_5_103'),
			'access_token' => Config::get('const.TOKEN_ROBOT')
		];

		return self::request($request_params, 'wall.getById');
	}

	/**
	 * Get comments by post id
	 * @param $post_id
	 * @return mixed
	 */
	public static function getCommentsData($post_id)
	{
		$request_params = [
			'owner_id' => -Config::get('const.GROUP_ID'),
			'post_id' => $post_id,
			'v' => Config::get('const.API_V_5_103'),
			'access_token' => Config::get('const.TOKEN_ROBOT')
		];
		return self::request($request_params, 'wall.getComments');
	}

	/**
	 * Check posts before deleting. Return ids for delete.
	 * @param $post_ids
	 * @return \Illuminate\Support\Collection
	 */
	public static function checkPostBeforeDelete($post_ids)
	{
		$data = self::getPostsData($post_ids);

		$data = collect($data);
		$delete_ids = collect();

		$data->each(
			function($post) use ($delete_ids) {
				$has_hashtag = self::hasHashtag($post->text);
				$has_links = self::hasLinks($post->text);
				$is_member = User::isMember($post->from_id);

				if (!$has_hashtag || $has_links || !$is_member) {
					$delete_ids->push($post->id);
				}
			}
		);

		return $delete_ids;
	}

	public static function approvePost($post_id)
	{
		$data = self::getCommentsData($post_id);

		if (!isset ($data->items)) {
			return;
		}

		$comments = collect($data->items);

		$comments->each(
			function ($comment) {
				if (User::isRobot($comment->from_id)) {
					Wall::deleteComment($comment->id);
				}
			}
		);
	}

	/**
	 * Get array of categories
	 * @return array
	 */
	private static function getCategories()
	{
		return [
			"0" => [
				"Одежда и обувь",
				"Женская одежда и обувь",
				"Мужская одежда и обувь",
				"Детская одежда и обувь"
			],
			"1" => [
				"Личные вещи",
				"Сумки, кошельки",
				"Бижутерия, драгоценности",
				"Часы",
				"Предметы гигиены",
				"Мелкие вещи",
				"Прочее личные вещи"
			],
			"2" => [
				"Электроника",
				"Компьютеры",
				"Ноутбуки",
				"Оргтехника и расходники",
				"Игровые приставки",
				"Компьютерные комплектующие",
				"Планшеты",
				"Мобильные и смартфоны",
				"Фото-, видео-, аудиотехника",
				"Телевизоры и ТВ-приставки",
				"Аксессуары для электроники",
				"Прочее электроника"
			],
			"3" => [
				"Бытовая техника",
				"Холодильники",
				"Плиты, духовки, СВЧ-печи",
				"Стиральные машины",
				"Посудомоечные машины",
				"Пылесосы",
				"Телевизоры и ТВ-приставки",
				"Мелкая бытовая техника",
				"Прочее бытовая техника"
			],
			"4" => [
				"Для дома и дачи",
				"Мебель",
				"Интерьер",
				"Телевизоры и ТВ-приставки",
				"Бытовая техника",
				"Инструмент",
				"Стройматериалы",
				"Всё для ремонта",
				"Посуда",
				"Кухонные принадлежности",
				"Для ванной",
				"Сад и огород",
				"Прочее для дома"
			],
			"5" => [
				"Для детей",
				"Для мам и грудных детей",
				"Детская одежда и обувь",
				"Игрушки",
				"Детская мебель",
				"Автокресла, переноски",
				"Коляски, санки",
				"Велосипеды детские",
				"Прочее для детей"
			],
			"6" => [
				"Животные и все для них",
				"Животные",
				"Товары для животных",
				"Корм для животных"
			],
			"7" => [
				"Растения и все для них",
				"Растения",
				"Товары для растений"
			],
			"8" => [
				"Авто-мото",
				"Автомобили",
				"Мотоциклы",
				"Мототехника",
				"Сельхозтехника",
				"Запчасти и комплектующие"
			],
			"9" => [
				"Недвижимость",
				"Продам жилую недвижимость",
				"Сдам жилую недвижимость",
				"Куплю жилую недвижимость",
				"Сниму жилую недвижимость",
				"Продам нежилую недвижимость",
				"Сдам нежилую недвижимость",
				"Куплю нежилую недвижимость",
				"Сниму нежилую недвижимость"
			],
			"10" => [
				"Активный отдых",
				"Охота и рыбалка",
				"Спорт",
				"Хобби",
				"Прочее активный отдых"
			],
			"11" => [
				"Красота и здоровье",
				"Техника для ухода за собой",
				"Медицина",
				"Парфюмерия и косметика",
				"Бижутерия, драгоценности",
				"Часы"
			],
			"12" => [
				"Разное",
				"Музыкальные инструменты",
				"Коллекционирование",
				"Ручная работа",
				"Книги и журналы",
				"Продукты питания"
			],
			"13" => [
				"Куплю",
				"Ищу"
			]
		];
	}
}
