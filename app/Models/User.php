<?php

namespace App\Models;


use App\Eloquent\Member;
use Illuminate\Support\Facades\Config;

class User extends Vk
{

	/**
	 * Base request for class User
	 * @param $params
	 * @param $method
	 * @return mixed
	 */
	public static function request($params, $method)
	{
		$base_params = [
			'access_token' => Config::get('const.TOKEN_ROBOT'),
			'v' => Config::get('const.API_V_5_103')
		];
		$request_params = array_merge($base_params, $params);

		return Vk::request($request_params, $method);
	}

	/**
	 * Get information about user
	 * @param $user_ids
	 * @param array $params
	 * @return mixed
	 */
	public static function get($user_ids, $params = [])
	{
		$base_params = [
			'user_ids' => $user_ids,
		];
		$request_params = array_merge($base_params, $params);

		return self::request($request_params, 'users.get');
	}

	/**
	 * Get array names user by id
	 * @param $user_id
	 * @return array
	 */
	public static function getName($user_id)
	{
		$result = [
			'first_name' => '',
			'last_name' => '',
		];
		$response = self::get($user_id);

		if (isset($response->first_name)) {
			$result['first_name']= $response->first_name;
		}

		if (isset($response->last_name)) {
			$result['last_name']= $response->last_name;
		}

		return $result;
	}

	/**
	 * Get information about friends
	 * @param $user_id
	 * @param array $params
	 * @return mixed
	 */
	public static function getFriends($user_id, $params = [])
	{
		$base_params = [
			'user_id' => $user_id,
			'order' => 'random',
			'fields' => 'city',
		];

		$request_params = array_merge($base_params, $params);

		return self::request($request_params, 'friends.get');
	}

	/**
	 * Get aggregate data about friends
	 * @param $user_id
	 * @return array|boolean
	 */
	public static function getFriendsData($user_id)
	{
		$result = [
			'count' => null,
			'count_any_city' => null,
			'count_city' => null,
			'percent' => null
		];

		$data = self::getFriends($user_id);

		$hasAnyCity = collect($data->items)->filter(
			function ($friend) {
				return isset($friend->city->id);
			}
		);

		$hasCity = collect($data->items)->filter(
			function ($friend) {
				return isset($friend->city->id) && $friend->city->id == Config::get('const.CITY_ID');
			}
		);

		$result['count'] = $data->count;
		$result['count_any_city'] = $hasAnyCity->count();
		$result['count_city'] = $hasCity->count();
		$result['percent'] = round($hasCity->count() * 100 / $hasAnyCity->count(), 2);

		$check_approve = self::checkApprove($result);

		$result = array_merge($result, $check_approve);

		return $result;
	}

	/**
	 * Check for friends about approve
	 * @param $data
	 * @return array
	 */
	private static function checkApprove($data)
	{
		$result = [
			'approve' => false,
			'reason' => ''
		];

		if ($data['count'] < Config::get('const.MIN_QTY_FRIENDS')) {
			$result['approve'] = false;
			$result['reason'] = 'Мало друзей';
		} elseif ($data['percent'] < Config::get('const.MIN_PERCENT_FRIENDS')) {
			$result['approve'] = false;
			$result['reason'] = 'Низкий процент друзей из города';
		} else {
			$result['approve'] = true;
		}

		return $result;
	}

	/**
	 * Remove member from group
	 * @param $user_id
	 */
	public static function remove($user_id)
	{
		$params = [
			'group_id' => Config::get('const.GROUP_ID'),
			'user_id' => $user_id,
			'v' => Config::get('const.API_V_5_63'),
			'access_token' => Config::get('const.TOKEN_ADMIN')
		];
		self::request($params, 'groups.removeUser');
	}

	/**
	 * Approve user join in group
	 * @param $user_id
	 */
	public static function approve($user_id)
	{
		$params = [
			'group_id' => Config::get('const.GROUP_ID'),
			'user_id' => $user_id,
			'v' => Config::get('const.API_V_5_63'),
			'access_token' => Config::get('const.TOKEN_ADMIN')
		];

		self::request($params, 'groups.approveRequest');
	}

	/**
	 * Check is user member already
	 * @param $user_id
	 * @return bool
	 */
	public static function isMember($user_id)
	{
		$params = [
			'group_id' => Config::get('const.GROUP_ID'),
			'user_id' => $user_id,
			'extended' => 1,
			'v' => Config::get('const.API_V_5_103'),
			'access_token' => Config::get('const.TOKEN_ROBOT')
		];
		$result = self::request($params, 'groups.isMember');

		if (isset($result->error)) {
			return true; // Error, but try to approve
		}

		if (isset($result->member) && $result->member) {
			return true;
		}
		return false;
	}

	/**
	 * @param $user_id
	 * @return Member | null
	 */
	public static function findOrCreateById($user_id)
	{
		$response = self::get($user_id, ['fields' => 'city']);

		if (isset($response[0])) {
			$response = $response[0];
		} else {
			$user = Member::firstOrCreate(['id' => $user_id]);
			return $user;
		}

		$is_member = self::isMember($user_id);

		$data = [
			'id' => $user_id,
			'first_name' => $response->first_name,
			'last_name' => $response->last_name,
			'is_member' => $is_member
		];

		if (isset($response->city->title)) {
			$data['city'] = $response->city->title;
		}

		$user = Member::updateOrCreate(['id' => $user_id], $data);

		return $user;

	}

	/**
	 * Is management by user id
	 * @param $user_id
	 * @return bool
	 */
	public static function isManager($user_id)
	{
		$managers = [Config::get('const.ADMIN_ID'), Config::get('const.ROBOT_ID'), -Config::get('const.GROUP_ID')];
		if (in_array($user_id, $managers)) {
			return true;
		}
		return false;
	}

	/**
	 * Is robot by user id
	 * @param $user_id
	 * @return bool
	 */
	public static function isRobot($user_id)
	{
		return $user_id == Config::get('const.ROBOT_ID');
	}
}
