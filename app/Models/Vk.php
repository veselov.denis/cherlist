<?php

namespace App\Models;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class Vk
{
	const API_V_5_103 = '5.103';
	const API_V_5_63 = '5.63';
	const API_V_5_9 = '5.9';

	/**
	 * Base request
	 * @param $params
	 * @param $method
	 * @return mixed
	 */
	public static function request($params, $method)
	{
		$base_params = [
			'access_token'	=> Config::get('const.TOKEN_GROUP'),
			'v'	=> Config::get('const.API_V_5_63')
		];

		$request_params = array_merge($base_params, $params);

		$response = Http::get("https://api.vk.com/method/".$method."?" . http_build_query($request_params))->object();

		if (isset($response->response)) {
			$response = $response->response;
		}

		return $response;
	}
}
