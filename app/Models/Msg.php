<?php

namespace App\Models;


use Illuminate\Support\Facades\Config;

class Msg extends Vk
{

	/**
	 * Seng message
	 * @param $msg
	 * @param $to_id
	 * @return mixed
	 */
	public static function send($msg, $to_id)
	{
		$params = [
			'message' => $msg,
			'user_id' => $to_id,
			'access_token'	=> Config::get('const.TOKEN_GROUP'),
		];

		return self::request($params, 'messages.send');
	}

	/**
	 * Send message to admin
	 * @param $msg
	 * @return mixed
	 */
	public static function sendAdmin($msg)
	{
		return self::send($msg, Config::get('const.ADMIN_ID'));
	}

	/**
	 * Send message to robot
	 * @param $msg
	 * @return mixed
	 */
	public static function sendRobot($msg)
	{
		return self::send($msg, Config::get('const.ROBOT_ID'));
	}

}
