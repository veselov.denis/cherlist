<?php

namespace App\Models;

use App\Eloquent\Member;
use App\Eloquent\Post;
use Carbon\Carbon;
use App\Eloquent\Comment as Comm;
use Illuminate\Support\Facades\Config;

class Callback
{
	private $request;

	/**
	 * Callback constructor.
	 * @param $request
	 */
    public function __construct($request)
	{
		$this->request = $request;
	}

	/**
	 * Run
	 * @return mixed|string
	 */
	public function run()
	{
		switch ($this->request->type):
			case 'confirmation':
				return Config::get('const.VK_CONFIRMATION_CODE');
				break;
			case 'group_join':
				$this->groupJoin();
				break;
			case 'group_leave':
				$this->groupLeave();
				break;
			case 'wall_post_new':
				$this->wallPostNew();
				break;
			default:
				return 'ok';
		endswitch;
	}

	/**
	 * Group join
	 */
	private function groupJoin()
	{
		$object = $this->request->object;

		$user_id = $object['user_id'];
		$join_type = $object['join_type'];

		$msg = "Заявка в группу: vk.com/id$user_id";

		$member = Member::firstOrNew(['id' => $user_id]);
		$member->join_type = $join_type;

		$user_data = User::get($user_id, ['fields' => 'city']);

		if (isset($user_data[0])) {
			$response = $user_data[0];
		} else {
			$msg = "\nОшибка при получении данных о пользователе.";
			Msg::sendAdmin($msg);
			Msg::sendRobot($msg);
			return;
		}

		$first_name = isset ($response->first_name) ? $response->first_name : '';
		$last_name = isset ($response->last_name) ? $response->last_name : '';

		$msg .= "\nПолучены данные: $first_name $last_name";
		$member->first_name = $first_name;
		$member->last_name = $last_name;

		if (isset($response->is_closed)) {
			if ($response->is_closed) {
				$msg .= "\nПрофиль закрыт";
			} else {
				$msg .= "\nПрофиль не закрыт";
			}
		} else {
			$msg .= "\nНет данных о доступности профиля.";
		}

		$city = null;
		if (isset($response->city)) {
			$city = $response->city;
			$msg .= "\nГород: $city->title";
			$member->city = $city->title;

			if ($city->title == "Череповец" || $city->id == Config::get('const.CITY_ID')) {
				$member->needed_check = true;
			}

		} else {
			$msg .= "\nГород недоступен";
		}

		$friends_data = false;
		if (isset($response) && !$response->is_closed && $response->can_access_closed) {
			$friends_data = User::getFriendsData($user_id);
		}

		if ($friends_data === false) {
			$msg .= "\nНе удалось проанализировать друзей. \nЗаявка не отклонена, но требует проверки.";
			$member->needed_check = true;
		} else {
			$msg .= "\nДрузей: {$friends_data['count']}";
			$msg .= "\nДрузей с городом: {$friends_data['count_any_city']}";
			$msg .= "\nИз города: {$friends_data['count_city']}";
			$msg .= "\nПроцент: {$friends_data['percent']}";

			if (!$friends_data['approve']) {
				User::remove($user_id);
				$msg .= "\nЗаявка отклонена. Причина: {$friends_data['reason']}";
				$member->is_member = false;
				$member->leave_type = Member::LEAVE_TYPE_ROBOT;
				$member->leave_reason = $friends_data['reason'];
				$member->leaved_at = Carbon::now();
			} else {
				$isMember = User::isMember($user_id);
				if ($isMember) {
					$member->is_member = true;
					$msg .= "\nУже состоит в группе";
				} else {
					$member->is_member = true;
					User::approve($user_id);
					$msg .= "\nДобавлен в группу";
				}
			}
		}

		Msg::sendRobot($msg);
		Msg::sendAdmin($msg);

		$member->save();

		return 'ok';
	}

	/**
	 * Group leave
	 */
	private function groupLeave()
	{
		$object = $this->request->object;

		$user_id = $object['user_id'];
		$self = $object['self'];

		$msg = "Покинул группу: vk.com/id$user_id";
		if ($self) {
			$msg .= " (самостоятельно)";
		}

		$member = Member::firstOrNew(['id' => $user_id]);

		if ($self) {
			$member->leave_type = Member::LEAVE_TYPE_SELF;
		} else {
			$member->leave_type = Member::LEAVE_TYPE_ROBOT;
		}

		$user_data = User::get($user_id, ['fields' => 'city']);

		if (isset($user_data[0])) {
			$response = $user_data[0];
		} else {
			return;
		}

		$first_name = $response->first_name;
		$last_name = $response->last_name;

		$msg .= "\nПолучены данные: $first_name $last_name";
		$member->first_name = $first_name;
		$member->last_name = $last_name;

		if (isset($response->is_closed)) {
			if ($response->is_closed) {
				$msg .= "\nПрофиль закрыт";
			} else {
				$msg .= "\nПрофиль не закрыт";
			}
		} else {
			$msg .= "\nНет данных о доступности профиля.";
		}

		if (isset($response->city)) {
			$city = $response->city;
			$msg .= "\nГород: $city->title";
			$member->city = $city->title;
		} else {
			$msg .= "\nГород недоступен";
		}

		$member->needed_check = false;
		$member->is_member = false;
		$member->leave_reason = "Самостоятельно";
		$member->leaved_at = Carbon::now();

		Msg::sendRobot($msg);
		Msg::sendAdmin($msg);

		$member->save();

		return 'ok';
	}

	/**
	 * New post on wall
	 * @return string
	 */
	private function wallPostNew()
	{
		$object = $this->request->object;

		$author_id = $object['from_id'];
		$post_id = $object['id'];
		$text = $object['text'];
		$date = $object['date'];

		if (!User::isManager($author_id)) {
			return 'ok';
		}

		$member = User::findOrCreateById($author_id);

		if ($member->is_banned) {
			sleep(3);
			Wall::deletePost($post_id);
		}

		$post = Post::firstOrCreate([
			'id' => $post_id,
			'text' => $text,
			'author_id' => $member->id,
		]);

		$has_links = Wall::hasLinks($text);
		$has_hashtag = Wall::hasHashtag($text);

		if ($has_links) {
			sleep(3);
			Wall::deletePost($post_id);
		}

		if (!$has_hashtag) {
			$msg = "В тексте объявления нет хештегов либо они написаны неверно.";
			$msg .= "\nИсправьте это, пожалуйста.";
			$msg .= "\nИначе робот удалит это сообщение через 30 минут.";
			$msg .= "\nСкопировать подходящие хештеги можно отсюда: vk.cc/6609qi";
			Wall::addComment($msg, $post_id);

			$post->date_must_delete = Carbon::now()->addMinutes(30);
		}

		if (!$member->is_member) {
			$msg = "Вы не состоите в группе, поэтому объявление будет удалено роботом через 30 минут";
			$msg .= "\nВступите в группу, чтобы этого избежать.";
			Wall::addComment($msg, $post_id);

			$post->date_must_delete = Carbon::now()->addMinutes(30);
		}
		$post->save();
	}
}
