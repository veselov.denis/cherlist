<?php

namespace App\Http\Controllers;


use App\Eloquent\Post;
use App\Models\Wall;
use Carbon\Carbon;

class CronController extends Controller
{
    public function removeWrongPosts()
	{
		$posts = Post::where('date_must_delete', '<', Carbon::now())->get();

		if ($posts->isEmpty()) {
			return 'ok';
		}

		$post_ids = $posts->pluck('id');

		$delete_ids = Wall::checkPostBeforeDelete($post_ids);

		// Posts must be deleted
		$delete_posts = $posts->filter(
			function ($post) use ($delete_ids) {
				return $delete_ids->contains($post->id);
			}
		);

		// Posts must be approved
		$approve_posts = $posts->filter(
			function ($post) use ($delete_ids) {
				return !$delete_ids->contains($post->id);
			}
		);

		// Delete
		$delete_posts->each(
			function ($post) {
				Wall::deletePost($post->id);
				$post->delete();
			}
		);

		// Approve
		$approve_posts->each(
			function ($post) {
				Wall::approvePost($post->id);
				$post->date_must_delete = null;
				$post->save();
			}
		);

		return 'ok';
	}
}
