<?php

namespace App\Http\Controllers;

use App\Models\Callback;
use Illuminate\Http\Request;

class VkController extends Controller
{
    public function callback(Request $request)
	{
		$callback = new Callback($request);
		$result = $callback->run();

		if (empty($result)) {
			$result = 'ok';
		}

		return $result;
	}
}
