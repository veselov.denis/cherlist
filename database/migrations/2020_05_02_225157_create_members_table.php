<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigInteger('id')->unique();
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('city')->nullable();
			$table->boolean('needed_check')->default(false);
			$table->boolean('is_banned')->default(false);
			$table->boolean('is_member')->default(true);
			$table->string('join_type')->nullable();
			$table->string('leave_type')->nullable();
			$table->string('leave_reason')->nullable();
			$table->timestamp('leaved_at')->nullable();

			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
