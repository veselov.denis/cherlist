<?php

use App\Models\User;
use App\Models\Vk;
use Illuminate\Database\Seeder;

class UserVkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		User::findOrCreateById(Config::get('const.ROBOT_ID'));
		User::findOrCreateById(Config::get('const.ADMIN_ID'));
    }
}
